require 'net/http'
require 'json'

BASE_URL = "http://brookcafe.co.uk"
BASIC_AUTH_USERNAME = 'GQ7S5BCH5QQMNM2VWTDQLKMS2VJUSG8K'
CURRENT_STATES = {"2" => "Payment accepted",  "3" => "Preparation in progress", "5" => "Delivered", "15" => "Order being processed", "16" => "Ready for collection", "17" => "Order collected"}

SCHEDULER.every '10s' do

  processOrders = getOrderDetails(formURL(BASE_URL, "15|16", 10 ))
  newOrders = getOrderDetails(formURL(BASE_URL, "2|3|5", 4 ))

  begin
    displayOrderToProcess(processOrders, createDashboardView(newOrders).length)
    displayNewOrders(newOrders)

  rescue => error
    puts error.backtrace
    raise # always reraise
  end
end

def displayNewOrders(res)

  orders = createDashboardView(res)

  orders.each_with_index { |(key,value),index|
    send_event("incomingItems#{index}", { item: value })
  }

end

def displayOrderToProcess(res, count)
  orders = createDashboardView(res, count)
  send_event('orderItems', { items: orders.values })
end


def createDashboardView(res, newOrderCount = 0)

  endpoints = Hash.new
  if res.length > 0
  orders = res['orders']

  orders.each_index { |index|
    endpoints[index] = addItems(orders[index], { label: orders[index]['reference'], value:  convertCurrentState(orders[index]['current_state']), paymentMethod: orders[index]['module'], pickup: getPickUp(orders[index]['id_carrier'])  })
  }
  elsif res.length == 0 && newOrderCount > 0
    endpoints[0] = { error: "There are incoming orders waiting to be processed"}
  else
    endpoints[0] = { error: "No orders available for processing"}
  end

  return endpoints
end

def getOrderDetails(url)
  uri = URI(url)

  Net::HTTP.start(uri.host, uri.port,
                  :use_ssl => uri.scheme == 'https',
                  :verify_mode => OpenSSL::SSL::VERIFY_NONE) do |http|

    request = Net::HTTP::Get.new uri.request_uri
    request.basic_auth(BASIC_AUTH_USERNAME, '')

    response = http.request request # Net::HTTPResponse object

    return JSON.parse(response.body)
  end
end

def formURL(url, states, limit)
  return "%s/api/orders?display=full&sort=date_upd_DESC&filter[current_state]=[%s]&limit=%d&output_format=JSON&date=1" % [url, states, limit]
end

def addItems(orders, items)
  begin
    return items.merge({:size => "%d Items" % [totalQuantity(orders['associations']['order_rows'])]})
  rescue
    return items
  end
end

def totalQuantity(products)
  quantity = 0
  products.each_index { |index|
    quantity += products[index]['product_quantity'].to_i
  }
  return quantity
end

def convertCurrentState(state)
  return CURRENT_STATES[state]
end

def getPickUp(method)
  if method == "18"
    "First"
  else
    "Second"
  end
end


